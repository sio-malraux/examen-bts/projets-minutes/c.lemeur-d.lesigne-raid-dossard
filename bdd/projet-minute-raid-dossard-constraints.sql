ALTER TABLE ONLY "raid_dossard"."activite_raid"
    ADD CONSTRAINT "pk_activite_raid" PRIMARY KEY ("code_raid", "code_activite");
ALTER TABLE ONLY "raid_dossard"."activite"
    ADD CONSTRAINT "pk_activite" PRIMARY KEY ("code");
ALTER TABLE ONLY "raid_dossard"."coureur"
    ADD CONSTRAINT "pk_coureur" PRIMARY KEY ("licence");
ALTER TABLE ONLY "raid_dossard"."equipe"
    ADD CONSTRAINT "pk_equipe" PRIMARY KEY ("numero", "code_raid");
ALTER TABLE ONLY "raid_dossard"."integrer_equipe"
    ADD CONSTRAINT "pk_integrer" PRIMARY KEY ("numero_equipe", "code_raid", "licence");
ALTER TABLE ONLY "raid_dossard"."raid"
    ADD CONSTRAINT "pk_raid" PRIMARY KEY ("code");

ALTER TABLE ONLY "raid_dossard"."activite_raid"
    ADD CONSTRAINT "activite_raid_code_activite_fkey" FOREIGN KEY ("code_activite") REFERENCES "raid_dossard"."activite"("code");
ALTER TABLE ONLY "raid_dossard"."activite_raid"
    ADD CONSTRAINT "activite_raid_code_raid_fkey" FOREIGN KEY ("code_raid") REFERENCES "raid_dossard"."raid"("code");
ALTER TABLE ONLY "raid_dossard"."equipe"
    ADD CONSTRAINT "equipe_code_raid_fkey" FOREIGN KEY ("code_raid") REFERENCES "raid_dossard"."raid"("code");
ALTER TABLE ONLY "raid_dossard"."integrer_equipe"
    ADD CONSTRAINT "inscrire_individuel_code_coureur_fkey" FOREIGN KEY ("licence") REFERENCES "raid_dossard"."coureur"("licence");
ALTER TABLE ONLY "raid_dossard"."integrer_equipe"
    ADD CONSTRAINT "inscrire_individuel_equipe_fkey" FOREIGN KEY ("numero_equipe", "code_raid") REFERENCES "raid_dossard"."equipe"("numero", "code_raid");
