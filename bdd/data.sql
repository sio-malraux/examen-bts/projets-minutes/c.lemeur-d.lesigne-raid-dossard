-- insertions dans la table activite :
insert into "raid_dossard".activite values(1,'VTT');
insert into "raid_dossard".activite values(2,'Cheval');
insert into "raid_dossard".activite values(3,'Orientation');


-- insertions dans la table raid :
insert into "raid_dossard".raid (code, nom, date_debut,ville,region,nb_maxi_par_equipe, montant_inscription, duree_maxi, age_minimum) values
('FR01','Velo','15/06/2020','Le Mans','Pays De La Loire',2,10.00,5,18),
('FR02','Poney','10/06/2020','Allonnes','Pays De La Loire',3,18.50,7,16);

-- insertions dans la table activite_raid :
insert into "raid_dossard".activite_raid values('FR01','1');
insert into "raid_dossard".activite_raid values('FR02','2');


-- insertions dans la table coureur :
insert into "raid_dossard".coureur values('L1','Le Meur','Clara','F','Francais','c.lemeur@mail.fr',true,'31-12-2000');
insert into "raid_dossard".coureur values('L2','Lesigne','Donovan','M','Francais','l.lesigne@mail.fr',true,'02-04-1998');
insert into "raid_dossard".coureur values('L3','Tazir','Valentin','M','Francais','v.tazir@mail.fr',true,'25-06-2000');
insert into "raid_dossard".coureur values('L4','Verrier','Dorian','M','Francais','d.verrier@mail.fr',true,'02-04-2000');
insert into "raid_dossard".coureur values('L5','Sidia','Aminata','F','Francais','a.sidia@mail.fr',true,'05-11-2000');
insert into "raid_dossard".coureur values('L6','Bouchet','Antoine','M','Francais','l.lesigne@mail.fr',true,'06-07-2000');
insert into "raid_dossard".coureur values('L7','Taysse','Geraldine','F','Francais','g.taysse@mail.fr',true,'16-04-1988');
insert into "raid_dossard".coureur values('L8','David','Gregory','M','Francais','g.david@mail.fr',true,'02-04-1988');
insert into "raid_dossard".coureur values('L9','Marie','Lana','F','Francais','m.lana@mail.fr',true,'25-09-1980');
insert into "raid_dossard".coureur values('L10','Rouge','Jean-Michel','M','Francais','j.rouge@mail.fr',true,'30-10-1970');

-- insertions dans la table equipe :
insert into "raid_dossard".equipe (numero,code_raid,nom) values('1','FR01','bleu');
insert into "raid_dossard".equipe (numero,code_raid,nom) values('2','FR01','blanc');
insert into "raid_dossard".equipe (numero,code_raid,nom) values('3','FR01','rouge');
insert into "raid_dossard".equipe (numero,code_raid,nom) values('1','FR02','violet');
insert into "raid_dossard".equipe (numero,code_raid,nom) values('2','FR02','rose');


-- insertions dans la table integrer_equipe :
insert into "raid_dossard".integrer_equipe (numero_equipe,code_raid,licence) values('1','FR01','L1');
insert into "raid_dossard".integrer_equipe (numero_equipe,code_raid,licence) values('1','FR01','L2');
insert into "raid_dossard".integrer_equipe (numero_equipe,code_raid,licence) values('2','FR01','L3');
insert into "raid_dossard".integrer_equipe (numero_equipe,code_raid,licence) values('2','FR01','L4');
insert into "raid_dossard".integrer_equipe (numero_equipe,code_raid,licence) values('1','FR02','L5');
insert into "raid_dossard".integrer_equipe (numero_equipe,code_raid,licence) values('1','FR02','L6');
insert into "raid_dossard".integrer_equipe (numero_equipe,code_raid,licence) values('1','FR02','L7');
insert into "raid_dossard".integrer_equipe (numero_equipe,code_raid,licence) values('2','FR02','L8');
