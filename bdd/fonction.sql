SET SCHEMA 'raid_dossard';

CREATE OR REPLACE FUNCTION test_age(varchar,int,varchar) RETURNS boolean AS
-- varchar : code_raid du raid
-- int : numero_equipe de l'équipe
-- varchar : licence du coureur
$BODY$
    DECLARE
	age_min int;
	age_coureur int;
    BEGIN
        SELECT INTO age_min age_minimum FROM raid_dossard.raid WHERE code = $1;

        SELECT INTO age_coureur extract('year' from age(NOW(),date_naissance)) FROM raid_dossard.coureur
        LEFT JOIN raid_dossard.integrer_equipe ON coureur.licence = integrer_equipe.licence
        WHERE integrer_equipe.numero_equipe = $2 AND integrer_equipe.code_raid =$1 AND integrer_equipe.licence = $3;

        IF(age_coureur>=age_min) THEN
		RETURN TRUE;
	ELSE
		RETURN FALSE;
	END IF;
    END
$BODY$
LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION test_equipe_complete(varchar,int) RETURNS boolean AS
-- varchar : code_raid du raid
-- int : numero_equipe de l'équipe
$BODY$
    DECLARE
	nb_max int;
	nb_joueur int;
    BEGIN
        SELECT INTO nb_joueur count(licence) FROM raid_dossard.integrer_equipe
        WHERE code_raid = $1 AND numero_equipe = $2;

        SELECT INTO nb_max nb_maxi_par_equipe FROM raid_dossard.raid
        WHERE code = $1;

        IF(nb_joueur < nb_max)THEN
        	RETURN FALSE;
        ELSE IF (nb_joueur = nb_max)THEN
		RETURN TRUE;
	     ELSE
		RETURN FALSE;
	     END IF;
	END IF;
    END
$BODY$
LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION test_femmes_equipe(varchar,int) RETURNS boolean AS
-- varchar : code_raid du raid
-- int : numero_equipe de l'équipe
$BODY$
    DECLARE
	nb_femmes_dans_equipe int;
	nb_femmes_min int;
    BEGIN
	SELECT INTO nb_femmes_dans_equipe count(sexe) FROM raid_dossard.coureur
	LEFT JOIN raid_dossard.integrer_equipe ON coureur.licence = integrer_equipe.licence
	WHERE integrer_equipe.numero_equipe = $2 AND integrer_equipe.code_raid = $1 AND coureur.sexe = 'F';

--	SELECT INTO nb_femmes_min nb_femmes FROM raid_dossard.raid WHERE code = $1;
	nb_femmes_min=1;
	IF(nb_femmes_dans_equipe < nb_femmes_min)THEN
		RETURN FALSE;
	ELSE
		RETURN TRUE;
	END IF;
    END
$BODY$
LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION test_dossard_existant(varchar,int,varchar) RETURNS boolean AS
-- varchar : code_raid du raid
-- int : numero_equipe de l'équipe
-- varchar : licence du coureur
$BODY$
    DECLARE
	dossard varchar;
    BEGIN
        SELECT INTO dossard num_dossard FROM raid_dossard.integrer_equipe
        WHERE code_raid = $1 AND numero_equipe = $2 AND licence = $3;

        IF(dossard IS NULL)THEN
		      RETURN FALSE;
       	ELSE
	      	RETURN TRUE;
    	  END IF;
    END
$BODY$
LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION test_raid_existe(varchar) RETURNS boolean AS
-- varchar : code_raid du raid
$BODY$
    DECLARE
        val boolean;
    BEGIN
        SELECT INTO val EXISTS(SELECT * FROM raid_dossard.raid where code=$1);
		RETURN val;
    END
$BODY$
LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION test_generation_dossard() RETURNS TRIGGER
AS $BODY$
	BEGIN
		IF (raid_dossard.test_dossard_existant(NEW.code_raid,NEW.numero_equipe, NEW.licence) IS TRUE) THEN
			RAISE EXCEPTION 'le dossard existe deja pour le coureur : %', NEW.licence;
		ELSE
			RETURN NEW;
		END IF;
	END;
$BODY$
LANGUAGE 'plpgsql';

CREATE TRIGGER trigger_avant_add_integrer_equipe BEFORE UPDATE ON integrer_equipe
FOR EACH ROW
EXECUTE PROCEDURE test_generation_dossard();
