SET client_encoding = 'UTF8';
DROP SCHEMA "raid_dossard" CASCADE;
CREATE SCHEMA "raid_dossard";

CREATE TABLE "raid_dossard"."activite" (
    "code" character varying(5) NOT NULL,
    "libelle" character varying(50)
);
CREATE TABLE "raid_dossard"."activite_raid" (
    "code_raid" character varying(6) NOT NULL,
    "code_activite" character varying(5) NOT NULL
);
CREATE TABLE "raid_dossard"."coureur" (
    "licence" character varying(5) NOT NULL,
    "nom" character varying(50),
    "prenom" character varying(50),
    "sexe" character(1),
    "nationalite" character varying(50),
    "mail" character varying(50),
    "certif_med_aptitude" boolean DEFAULT false,
    "date_naissance" "date"
);
CREATE TABLE "raid_dossard"."equipe" (
    "numero" integer NOT NULL,
    "code_raid" character varying(6) NOT NULL,
    "nom" character varying(50),
    "date_inscription" "date" DEFAULT "now"(),
    "temps_global" integer DEFAULT 0,
    "classement" integer DEFAULT 0,
    "penalites_bonif" integer DEFAULT 0
);
CREATE TABLE "raid_dossard"."integrer_equipe" (
    "numero_equipe" integer NOT NULL,
    "code_raid" character varying(6) NOT NULL,
    "licence" character varying(5) NOT NULL,
    "temps_individuel" integer DEFAULT 0,
    "num_dossard" character varying(15)
);
CREATE TABLE "raid_dossard"."raid" (
    "code" character varying(6) NOT NULL,
    "nom" character varying(50),
    "date_debut" character varying(10),
    "ville" character varying(50),
    "region" character varying(50),
    "nb_maxi_par_equipe" integer,
    "montant_inscription" numeric(8,2),
    "nb_femmes" integer,
    "duree_maxi" integer,
    "age_minimum" integer
);
