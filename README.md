# Projet Raid Dossard
Ce projet a pour but de généré automatiquement des dossards
appartenant aux équipes qui participe au raid choisie.

Les Raids sont des compétitions sportives. 

## Licence
[AGPL v3](https://www.gnu.org/licenses/agpl-3.0.html)

## Cahier des Charges
Basé sur la version livrable
[0.3.1](https://framagit.org/sio/projets-minutes/projet-minute-raid-dossard/-/tags)

## Principales dépendances
- [Tclap](http://tclap.sourceforge.net/) pour la gestion des options
  de la ligne de commande
- [libpq](https://www.postgresql.org/docs/9.5/libpq.html) pour accéder
  à la base de donnée [PostgreSQL](https://www.postgresql.org/)
- [plantuml](https://plantuml.com/) pour la modélisation du diagramme
  de classe en UML
- [doxygen](https://www.doxygen.nl/) pour la génération de la
  documentation technique

## Doc utilisateur 
Nom de l'exécutable : `RaidDossard`

### Les paramètres
|Obligatoires|Optionnels|
|------------|--------------------------------------------------------------------------------|
|`--hostname`, `-H`|`--password`, `-W`, utilisation des informations contenues dans `~/.pgpass`par défaut |
|`--dbname`, `-d`  |`--port`, `-p`, 5432 par défaut |
|`--username`, `-U`|`--version` |
|`--raid`, `-r`    |`--help`, `-h` |
 
### Exemples
#### Accéder à l'aide en ligne
```bash
RaidDossard -h
```

#### Connection avec tous les paramètres
```bash
RaidDossard -H postgresql.fqdn.com -d public -U world -W mypassword -p 5432 -r FR01
```

#### Connection avec seulement les paramètres obligatoires
```bash
RaidDossard -H postgresql.fqdn.com -d public -U world -r FR01
```

#### Conservation du journal dans un fichier `journal.log`
```bash
RaidDossard -H postgresql.fqdn.com -d public -U world -r FR01 > journal.log
```

## Diagramme ULM des classes

![UML de Raid Dossard](doc/UML_raid_dossard.png)


## Pour cloner le dépôt
```bash
git clone https://framagit.org/sio-malraux/examen-bts/projets-minutes/c.lemeur-d.lesigne-raid-dossard.git
```
