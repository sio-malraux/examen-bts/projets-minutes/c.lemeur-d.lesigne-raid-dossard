#ifndef COUREUR_H
#define COUREUR_H
#include <iostream>

/**
 * \class Coureur
 * \brief definition of the class coureur
 */
class Coureur
{
public:

  /**
   * \brief Default constructor of a runner 
   */
  Coureur();
   
  /**
   * \brief Constructor with parameters
   * \param[in] Licence the licence number of a runner 
   * \param[in] Nom the name of a runner 
   * \param[in] Prenom the first name of a runner 
   * \param[in] Sexe the sex of a runner 
   * \param[in] Nationalite the nationality of a runner 
   * \param[in] Mail the mail of a runner 
   * \param[in] Certif_med_aptitude the medical certificate of aptitude of a runner 
   * \param[in] Date_naissance the date of birth of a runner 
   */
  Coureur(std::string,std::string,std::string,std::string,std::string,std::string,bool,std::string); 

  /**
   * \brief Default destructor of a runner 
   */
  ~Coureur();
   
  /**
   * \brief Allows reading a licence of a runner 

   * \return string representing the licence of a runner 
   */
  std::string getLicence();
 
  /**
   * \brief Allows reading a name of a runner 

   * \return string representing the name of a runner 
   */
  std::string getNom(); 

  /**
   * \brief Allows reading the first name of a runner 

   * \return string representing the first name of a runner 
   */
  std::string getPrenom(); 

  /**
   * \brief Allows reading the sex of a runner 

   * \return string representing the sex of a runner 
   */
  std::string getSexe(); 

  /**
   * \brief Allows reading a nationality of a runner 

   * \return string representing the nationality of a runner 
   */
  std::string getNationalite(); 

  /**
   * \brief Allows reading the mail of a runner 

   * \return string representing the mail of a runner 
   */ 
  std::string getMail(); 

  /**
   * \brief Allows reading the medical certificate of aptitude for the runner

   * \return bool representing the medical certificate of aptitude of a runner 
   */
  bool getCertif_med_aptitude(); 
   
  /**
   * \brief Allows reading date of birth for the runner

   * \return date representing the date of birth of a runner 
   */
  std::string getDate_naissance(); 
  
private:
  std::string licence;///< Attribute defining the licence of a runner
  std::string nom;///< Attribute defining the name of a runner
  std::string prenom;///< Attribute defining the first name of a runner
  std::string sexe;///< Attribute defining the sex of a runner
  std::string nationalite;///< Attribute defining the nationality of a runner
  std::string mail;///< Attribute defining the mail of a runner
  bool certif_med_aptitude;///< Attribute defining the medical certificate of aptitude of a runner
  std::string date_naissance;///< Attribute defining the date of birth of a runner
};
#endif // COUREUR_H
