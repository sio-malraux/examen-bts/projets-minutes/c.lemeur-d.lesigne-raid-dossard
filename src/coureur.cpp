#include "coureur.h"

Coureur::Coureur():
  licence(""),
  nom(""),
  prenom(""),
  sexe(""),
  nationalite(""),
  mail(""),
  certif_med_aptitude(true),
  date_naissance("2000-01-01")
{}

Coureur::Coureur(std::string _licence,std::string _nom,std::string _prenom,std::string _sexe,std::string _nationalite,std::string _mail,bool _certif_med_aptitude,std::string _date_naissance):
  licence(_licence),
  nom(_nom),
  prenom(_prenom),
  sexe(_sexe),
  nationalite(_nationalite),
  mail(_mail),
  certif_med_aptitude(_certif_med_aptitude),
  date_naissance(_date_naissance)
{}

Coureur::~Coureur()
{}

std::string Coureur::getLicence()
{
	return licence;
}

std::string Coureur::getNom()
{
	return nom;
}

std::string Coureur::getPrenom()
{
	return prenom;
}

std::string Coureur::getSexe()
{
	return sexe;
}

std::string Coureur::getNationalite()
{
	return nationalite;
}

std::string Coureur::getMail()
{
	return mail;
}

bool Coureur::getCertif_med_aptitude()
{
	return certif_med_aptitude;
}

std::string Coureur::getDate_naissance()
{
	return date_naissance;
}
