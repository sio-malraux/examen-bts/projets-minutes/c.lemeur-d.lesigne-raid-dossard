#include "equipe.h"

Equipe::Equipe():
  numero(0),
  code_raid(""),
  nom(""),
  date_inscription(""),
  temps_global(0),
  classement(0),
  penalites_bonif(0)
{}

Equipe::Equipe(int _numero,std::string _code_raid,std::string _nom, std::string _date_inscription,int _temps_global,int _classement, int _penalites_bonif):
  numero(_numero),
  code_raid(_code_raid),
  nom(_nom),
  date_inscription(_date_inscription),
  temps_global(_temps_global),
  classement(_classement),
  penalites_bonif(_penalites_bonif)
{}

Equipe::~Equipe()
{}

int Equipe::getNumero()
{
	return numero;
}

std::string Equipe::getCode_raid()
{
	return code_raid;
}

std::string Equipe::getNom()
{
	return nom;
}

std::string Equipe::getDate_inscription()
{
	return date_inscription;
}

int Equipe::getTemps_global()
{
	return temps_global;
}


int Equipe::getClassement()
{
  return classement;
}

int Equipe::getPenalites_bonif()
{
  return penalites_bonif;
}
