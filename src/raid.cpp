#include "raid.h"

Raid::Raid():
  code(""),
  nom(""),
  date_debut(""),
  ville(""),
  region(""),
  nb_maxi_par_equipe(0),
  montant_inscription(0.0),
  nb_femmes(0),
  duree_maxi(0),
  age_minimum(0)
{}

Raid::Raid(std::string _code,std::string _nom,std::string _date_debut,std::string _ville,std::string _region,int _nb_maxi_par_equipe,float _montant_inscription,int _nb_femmes,int _duree_maxi,int _age_minimum):
  code(_code),
  nom(_nom),
  date_debut(_date_debut),
  ville(_ville),
  region(_region),
  nb_maxi_par_equipe(_nb_maxi_par_equipe),
  montant_inscription(_montant_inscription),
  nb_femmes(_nb_femmes),
  duree_maxi(_duree_maxi),
  age_minimum(_age_minimum)
{}

Raid::~Raid()
{}

std::string Raid::getCode()
{
	return code;
}

std::string Raid::getNom()
{
	return nom;
}

std::string Raid::getDate_debut()
{
	return date_debut;
}

std::string Raid::getVille()
{
	return ville;
}

std::string Raid::getRegion()
{
	return region;
}

int Raid::getNb_maxi_par_equipe()
{
	return nb_maxi_par_equipe;
}

float Raid::getMontant_inscription()
{
	return montant_inscription;
}

int Raid::getNb_femmes()
{
	return nb_femmes;
}

int Raid::getDuree_maxi()
{
	return duree_maxi;
}

int Raid::getAge_minimum()
{
	return age_minimum;
}
