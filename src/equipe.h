#ifndef EQUIPE_H
#define EQUIPE_H
#include <iostream>

/**
 * \class Equipe
 * \brief definition of the class equipe
 */
class Equipe
{
public:

  /**
   * \brief Default constructor of a team 
   */
  Equipe();
  
  /**
   * \brief Constructor with parameters
   * \param[in] numero the numero of the team
   * \param[in] code_raid the code of a raid 
   * \param[in] nom the name of a team 
   * \param[in] date_inscription the day where the team is create
   * \param[in] temps_global the global time of a team 
   * \param[in] classement the result of a team at the end of the raid
   * \param[in] penalites_bonif the penality of the team
   */
  Equipe(int,std::string,std::string,std::string,int,int,int);
  
  /**
   * \brief Default destructor of a team 
   */
  ~Equipe();
   
  /**
   * \brief Allows reading the number of a team 
   * \return int representing the number of a team 
   */
  int getNumero();

  /**
   * \brief Allows reading a code of a raid 
   * \return string representing the code of a raid 
   */
  std::string getCode_raid();

  /**
   * \brief Allows reading the name of a team 
   * \return string representing the name of a team 
   */
  std::string getNom();

  /**
   * \brief Allows reading the day where the team is create
   * \return string representing the date of creation of the team
   */
  std::string getDate_inscription();
  
  /**
   * \brief Allows reading a global time of a team 
   * \return int representing the global time of a team 
   */
  int getTemps_global();

  /**
   * \brief Allows reading the place of a team at the end
   * \return int representing the place of a team at the end
   */
  int getClassement();

/**
 * \brief Allow reading the penalty of the team
 * \return int representing the penalty of the team
 */
  int getPenalites_bonif();
  
private:
  int numero;///< Attribute defining the team number of a team
  std::string code_raid;///< Attribute defining the code of a raid
  std::string nom;///< Attribute defining the name of a team
  std::string date_inscription;///< Attribute defining the day of the team creation
  int temps_global;///< Attribute defining the individual time of a team
  int classement;///< Attribute defining the place of the team at the end of the raid
  int penalites_bonif;///< Attribute defining the penality of the team
};
#endif // EQUIPE_H
