#include <iostream>
#include <libpq-fe.h>
#include <tclap/CmdLine.h>
#include <log4cxx/logger.h>
#include <log4cxx/logmanager.h>
#include <log4cxx/basicconfigurator.h>

static log4cxx::LoggerPtr logger(log4cxx::Logger::getLogger("Main"));

int main(int argc, char *argv[])
{
  if(!log4cxx::LogManager::getLoggerRepository()->isConfigured())
  {
    log4cxx::BasicConfigurator::configure();
  }
  logger->setLevel(log4cxx::Level::getDebug());
  try {
    TCLAP::CmdLine cmd("To generate the jersey for a raid evenement. The teams must be complete and the riders must not have a jersey.",' ',"0.2",true);
    TCLAP::ValueArg<std::string> hostArg("H","hostname","Specifies the host name of the machine on which the server is running.",true,"","string");
    TCLAP::ValueArg<std::string> nameArg("d","dbname","Specifies the name of the database to connect to.",true,"","string");
    TCLAP::ValueArg<std::string> userArg("U","username","Connect to the database as the user username instead of the default. (You must have permission to do so, of course.)",true,"","string");
    TCLAP::ValueArg<std::string> passwordArg("W","password","Specifies the option: password to connecting to a database.",false,"","string");
    TCLAP::ValueArg<std::string> portArg("p","port","Specifies the TCP port or the local Unix-domain socket file extension on which the server is listening for connections.By default it's 5432.",false,"5432","string");
    TCLAP::ValueArg<std::string> raidArg("r","raid","Specifies the code of the raid you want to generate the jersey",true,"FR01","string");
    cmd.add( raidArg );
    cmd.add( portArg );
    cmd.add( passwordArg );
    cmd.add( userArg );
    cmd.add( nameArg );
    cmd.add( hostArg );
    cmd.parse(argc, argv);
    std::string host = hostArg.getValue();
    std::string name = nameArg.getValue();
    std::string user = userArg.getValue();
    std::string pass = passwordArg.getValue();
    std::string port = portArg.getValue();
    std::string raid = raidArg.getValue();
    std::string info_connexion = "host=" + host + " port='" + port + "' user=" + user + " dbname=" + name + " password=" + pass;
    PGconn *connexion = PQconnectdb(info_connexion.c_str());

    if(PQstatus(connexion) != CONNECTION_OK)
    {
      logger->fatal("Connection Failed");
    }
    else
    {
      logger->info("Connection success");

      //Test si le raid existe : use fonction sql : test_raid_existe(varchar);
      std::string rqt_exist = "SELECT raid_dossard.test_raid_existe('" + raid + "');";
      PGresult *res = PQexec(connexion, rqt_exist.c_str());

      if(PQresultStatus(res) == PGRES_TUPLES_OK)
      {
        if(std::string(PQgetvalue(res, 0, 0))=="f")
        {
          logger->error("le raid " + raid + " n'existe pas");
        }
        else if(std::string(PQgetvalue(res, 0, 0))=="t")
        {
          //Recuperer les equipes appartenant au raid choisie
          std::string rqt_equipe_raid = "SELECT DISTINCT numero_equipe FROM raid_dossard.integrer_equipe WHERE code_raid = '" + raid + "';";

          PGresult *res = PQexec(connexion, rqt_equipe_raid.c_str());
          if(PQresultStatus(res) == PGRES_TUPLES_OK)
          {
            //teste si les equipes du raid sont completent + BONUS si femmes dans l equipe;
            int nbLignes = PQntuples(res);
            int ligne,i,nbLicence;
            std::string rqt_equipe_complete, rqt_licence, rqt_femmes;
            for(ligne=0; ligne < nbLignes; ligne++)
            {
              rqt_equipe_complete = "SELECT raid_dossard.test_equipe_complete('" + raid + "'," + PQgetvalue(res,ligne,0) + ");";
              PGresult *res_equipe = PQexec(connexion, rqt_equipe_complete.c_str());
              if(PQresultStatus(res_equipe) == PGRES_TUPLES_OK)
              {
                if(std::string(PQgetvalue(res_equipe, 0, 0))=="t")
                {
                  //test si il y a au moins une femme dans lequipe
                  rqt_femmes = "SELECT raid_dossard.test_femmes_equipe('" + raid + "','" + PQgetvalue(res,ligne,0) + "');";
                  PGresult *res_femmes = PQexec(connexion, rqt_femmes.c_str());
                  if(PQresultStatus(res_femmes) == PGRES_TUPLES_OK)
                  {
                    if(std::string(PQgetvalue(res_femmes, 0, 0))=="t")
                    {
                      //recupere les licences des coureurs de l equipe
                      rqt_licence = "SELECT licence FROM raid_dossard.integrer_equipe WHERE code_raid='" + raid + "' AND numero_equipe=" + PQgetvalue(res,ligne,0) + ";";
                      PGresult *res_licence = PQexec(connexion, rqt_licence.c_str());
                      if(PQresultStatus(res_licence) == PGRES_TUPLES_OK)
                      {
                        nbLicence = PQntuples(res_licence);
                        //Tester si les dossards existent avec test_dossard_existant(varchar raid, int equipe, varchar licence)
                        for(i=0; i < nbLicence; i++)
                        {
                          std::string rqt_dossard="SELECT raid_dossard.test_dossard_existant('" + raid + "'," + PQgetvalue(res,ligne,0) + ",'" + PQgetvalue(res_licence,i,0) + "');";
                          PGresult *res_dossard = PQexec(connexion, rqt_dossard.c_str());
                          if(PQresultStatus(res_dossard) == PGRES_TUPLES_OK)
                          {
                            if(std::string(PQgetvalue(res_dossard, 0, 0))=="t")
                            {
                              logger->warn("dossard deja genere pour la licence :" + std::string(PQgetvalue(res_licence,i,0)));
                            }
                            else
                            {
                              std::string rqt_inital="SELECT CONCAT(UPPER(SUBSTR(prenom, 1, 1)), UPPER(SUBSTR(nom, 1, 1))) FROM raid_dossard.coureur WHERE licence='" + std::string(PQgetvalue(res_licence,i,0)) + "';";
                              PGresult *res_initial = PQexec(connexion, rqt_inital.c_str());
                              if(PQresultStatus(res_initial) != PGRES_TUPLES_OK)
                              {
                                logger->error("concernant la requete qui recupere les initiale du coureur " + std::string(PQgetvalue(res_licence,i,0)) + " pour le raid " + raid + " et l'equipe " + std::string(PQgetvalue(res,ligne,0)));
                              }
                              else
                              {
                                //Creation requete update
                                std::string num_dossard=std::string(PQgetvalue(res,ligne,0)) + "-" + raid + "-" + PQgetvalue(res_initial,0,0) + "-" + std::to_string(i+1);
                                std::string rqt_update="UPDATE raid_dossard.integrer_equipe set num_dossard ='" + num_dossard + "' where code_raid='" + raid + "' AND numero_equipe=" + PQgetvalue(res,ligne,0) + " AND licence='" + PQgetvalue(res_licence,i,0) + "';";
                                PGresult *res_update = PQexec(connexion, rqt_update.c_str());
                                if(PQresultStatus(res_update) != PGRES_TUPLES_OK)
                                {
                                  logger->error("concernant la requete qui upgrade le dossard pour le raid " + raid + ", l'equipe " + std::string(PQgetvalue(res,ligne,0)) + " et le coureur " + std::string(PQgetvalue(res_licence,i,0)));
                                }
                              }
                            }
                          }
                          else
                          {
                            logger->error("concernant la requete qui test si dossard est existant pour le coureur " + std::string(PQgetvalue(res_licence,i,0)));
                          }
                        }//fin boucle for
                      }
                      else
                      {
                        logger->error("concernant la requete qui recupere les licences des coureurs de l'equipe " + std::string(PQgetvalue(res,ligne,0)));
                      }
                    }
                    else
                    {
                      logger->warn("Il n'a a pas au moins une femmes dans l'equipe " + std::string(PQgetvalue(res,ligne,0)) + " du raid " + raid);
                    }
                  }
                  else
                  {
                    logger->error ("concernant la requete qui test si il y a au moins une femmes dans l'equipe " + std::string(PQgetvalue(res,ligne,0)));
                  }
                }
                else
                {
                  logger->warn("L'equipe " + std::string(PQgetvalue(res,ligne,0)) + " du raid " + raid + " n'est pas complete");
                }
              }
              else
              {
                logger->error("concernant la requete qui verifie que l'equipe " + std::string(PQgetvalue(res,ligne,0)) + " est complete");
              }
            }//fin boucle for
            logger->info("Fin les dossards sont generes pour le raid " + raid);
          }
          else
          {
            logger->error("concernant la requete qui recupere les equipes du raid choisie : " + raid);
          }
        }
      }
      else
      {
        logger->error("concernant la requete qui test si le raid " + raid + " exist");
      }
    }

  } catch (TCLAP::ArgException &e)  // catch any exceptions
  { std::cerr << "error: " << e.error() << " for arg " << e.argId() << std::endl; }

  return 0;
}
