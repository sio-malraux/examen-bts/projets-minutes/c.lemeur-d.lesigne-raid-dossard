#ifndef INTEGRE_EQUIPE_H
#define INTEGRE_EQUIPE_H
#include <iostream>

/**
 * \class Integrer_equipe
 * \brief definition of the class Integrer_equipe
 */
class Integrer_equipe
{
public:

  /**
   * \brief Default constructor of a team 
   */
  Integrer_equipe();
  
  /**
   * \brief Constructor with parameters
   * \param[in] num_Integrer_equipe the team number of a team 
   * \param[in] code_raid the code of a raid 
   * \param[in] licence the licence of a team 
   * \param[in] temps_individuel the individual time of a team 
   * \param[in] num_dossard the jersey number of a team
   */
  Integrer_equipe(int,std::string,std::string,int,std::string);
  
  /**
   * \brief Default destructor of a team 
   */
  ~Integrer_equipe();
   
  /**
   * \brief Allows reading a team number of a team 
   * \return int representing the team number of a team 
   */
  int getNumero_equipe();

  /**
   * \brief Allows reading a code of a raid 
   * \return string representing the code of a raid 
   */
  std::string getCode_raid();

  /**
   * \brief Allows reading the licence of a team 
   * \return string representing the licence of a team 
   */
  std::string getLicence();

  /**
   * \brief Allows reading a individual time of a team 
   * \return int representing the individual time of a team 
   */
  int getTemps_individuel();

  /**
   * \brief Allows reading a jersey number of a team 
   * \return int representing the jersey number of a team 
   */
  std::string getNum_dossard();

private:
  int numero_equipe;///< Attribute defining the team number of a team
  std::string code_raid;///< Attribute defining the code of a raid
  std::string licence;///< Attribute defining the licence of a team
  int temps_individuel;///< Attribute defining the individual time of a team
  std::string num_dossard;///< Attribute defining the jersey number of a team
};
#endif // INTEGRE_EQUIPE_H
