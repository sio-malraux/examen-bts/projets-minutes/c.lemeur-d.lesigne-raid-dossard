#ifndef RAID_H
#define RAID_H
#include <iostream>

/**
 * \class Raid
 * \brief definition of the class raid
 */
class Raid
{
public:

  /**
   * \brief Default constructor of a raid 
   */
  Raid();
   
  /**
   * \brief Constructor with parameters
   * \param[in] code the code number of a raid 
   * \param[in] Nom the name of a raid 
   * \param[in] Date_debut the start date of a raid 
   * \param[in] Ville the city of a raid 
   * \param[in] Region the region of a raid 
   * \param[in] Nb_maxi_par_equipe the number max per team of a raid 
   * \param[in] Montant_inscription the registration price of a raid 
   * \param[in] Nb_femmes the number of women of a raid 
   * \param[in] Duree_maxi the max duration of a raid 
   * \param[in] Age_minimum the minimum age of a raid 
   */
  Raid(std::string,std::string,std::string,std::string,std::string,int,float,int,int,int); 

  /**
   * \brief Default destructor of a raid 
   */
  ~Raid();
   
  /**
   * \brief Allows reading a code of a raid 

   * \return string representing the code of a raid 
   */
  std::string getCode();
 
  /**
   * \brief Allows reading a Name of a raid 

   * \return string representing the Name of a raid 
   */
  std::string getNom(); 

  /**
   * \brief Allows reading the start date of a raid 

   * \return string representing the start date of a raid 
   */
  std::string getDate_debut(); 

  /**
   * \brief Allows reading a city of a raid 

   * \return string representing the city of a raid 
   */
  std::string getVille(); 

  /**
   * \brief Allows reading a Region of a raid 

   * \return string representing the Region of a raid 
   */
  std::string getRegion(); 

  /**
   * \brief Allows reading the number max per team of a raid 

   * \return int representing the number max per team of a raid 
   */ 
  int getNb_maxi_par_equipe(); 

  /**
   * \brief registration price for the raid

   * \return float representing the registration price of a raid 
   */
  float getMontant_inscription(); 

  /**
   * \brief Allows reading the number of women of a raid 

   * \return int representing the number of women of a raid 
   */
  int getNb_femmes(); 

  /**
   * \brief Allows reading the max duration of a raid 

   * \return int representing the max duration of a raid 
   */
  int getDuree_maxi(); 

  /**
   * \brief Allows reading a minimum age of a raid 

   * \return int representing a minimum age of a raid 
   */
  int getAge_minimum();
  
private:
  std::string code;///< Attribute defining the code of a raid
  std::string nom;///< Attribute defining the name of a raid
  std::string date_debut;///< Attribute defining the date_debut of a raid
  std::string ville;///< Attribute defining the ville of a raid
  std::string region;///< Attribute defining the region of a raid
  int nb_maxi_par_equipe;///< Attribute defining the number max per team of a raid
  float montant_inscription;///< Attribute defining the registration price of a raid
  int nb_femmes;///< Attribute defining the number of women of a raid
  int duree_maxi;///< Attribute defining the max duration of a raid
  int age_minimum;///< Attribute defining the minimum age of a raid
};
#endif // RAID_H
