#include "integrer_equipe.h"

Integrer_equipe::Integrer_equipe():
  numero_equipe(0),
  code_raid(""),
  licence(""),
  temps_individuel(0),
  num_dossard("")
{}

Integrer_equipe::Integrer_equipe(int _numero_equipe,std::string _code_raid,std::string _licence,int _temps_individuel,std::string _num_dossard):
  numero_equipe(_numero_equipe),
  code_raid(_code_raid),
  licence(_licence),
  temps_individuel(_temps_individuel),
  num_dossard(_num_dossard)
{}

Integrer_equipe::~Integrer_equipe()
{}

int Integrer_equipe::getNumero_equipe()
{
	return numero_equipe;
}

std::string Integrer_equipe::getCode_raid()
{
	return code_raid;
}

std::string Integrer_equipe::getLicence()
{
	return licence;
}

int Integrer_equipe::getTemps_individuel()
{
	return temps_individuel;
}

std::string Integrer_equipe::getNum_dossard()
{
	return num_dossard;
}

